// Server.cpp by Walter <wxc@email.com> September 2001
#define _WIN32_DCOM
#include <iostream>

using namespace std;

#include "Server.h"
HANDLE g_hEvent;

class CMiniDcom : public IDouble
{
public:
	ULONG __stdcall AddRef() { return InterlockedIncrement(&m_cRef); };
	ULONG __stdcall Release();
	HRESULT __stdcall QueryInterface(REFIID iid, void** ppv);
	HRESULT __stdcall GetTypeInfoCount(UINT* pCountTypeInfo) {  *pCountTypeInfo = 1;  return S_OK; };
	HRESULT __stdcall GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo** ppITypeInfo);
	HRESULT __stdcall GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames, LCID lcid, DISPID* rgDispId)
   { return DispGetIDsOfNames(m_pTypeInfo, rgszNames, cNames, rgDispId); };
	HRESULT __stdcall Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pDispParams, VARIANT* pVarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr)
   { return DispInvoke(this, m_pTypeInfo, dispIdMember, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr); }
	HRESULT __stdcall Double(int x, int *retval);
	CMiniDcom() : m_cRef(1) { CoAddRefServerProcess();};
	~CMiniDcom() { if(CoReleaseServerProcess() == 0) SetEvent(g_hEvent); };
	long m_cRef;
	ITypeInfo* m_pTypeInfo;
};

HRESULT CMiniDcom::GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo** ppITypeInfo)
{
	*ppITypeInfo = NULL;
	m_pTypeInfo->AddRef();
	*ppITypeInfo = m_pTypeInfo;
	return S_OK;
}

ULONG CMiniDcom::Release()
{
	unsigned cRef = InterlockedDecrement(&m_cRef);
	if(cRef != 0) return cRef;
	m_pTypeInfo->Release(); 
	delete this;
	return 0;
}

HRESULT CMiniDcom::QueryInterface(REFIID iid, void **ppv)
{
	if(iid == IID_IUnknown)			*ppv = (IUnknown*)this;
	else if(iid == IID_IDouble)		*ppv = (IDouble*)this;
	else if(iid == IID_IDispatch)	*ppv = (IDispatch*)this; 
	else{
		*ppv = NULL;
		return E_NOINTERFACE;
	}
	AddRef();
	return S_OK;
}

HRESULT CMiniDcom::Double(int x, int *retval)
{
	cout << "Received:" << x << endl;
	*retval = x * 2;
	cout << "Returned:" << *retval << endl;
	return S_OK;
}

class CFactory : public IClassFactory
{
public:
	ULONG __stdcall AddRef() {return InterlockedIncrement(&m_cRef);};;
	ULONG __stdcall Release();
	HRESULT __stdcall QueryInterface(REFIID iid, void** ppv);
	HRESULT __stdcall CreateInstance(IUnknown *pUnknownOuter, REFIID iid, void** ppv);
	HRESULT __stdcall LockServer(BOOL bLock);
	CFactory() : m_cRef(1) { }
	~CFactory() {  }
private:
	long m_cRef;
};


ULONG CFactory::Release()
{
	unsigned cRef = InterlockedDecrement(&m_cRef);
	if(cRef != 0) return cRef;
	delete this;
	return 0;
}

HRESULT CFactory::QueryInterface(REFIID iid, void** ppv)
{
	if((iid == IID_IUnknown) || (iid == IID_IClassFactory))
		*ppv = (IClassFactory *)this;
	else 
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}
	AddRef();
	return S_OK;
}

HRESULT CFactory::CreateInstance(IUnknown *pUnknownOuter, REFIID iid, void **ppv)
{
	if(pUnknownOuter != NULL)	return CLASS_E_NOAGGREGATION;
	CMiniDcom *pMiniDcom = new CMiniDcom; 
	if(pMiniDcom == NULL) return E_OUTOFMEMORY;
	ITypeLib* pTypeLib;
	if(FAILED(LoadRegTypeLib(LIBID_Server, 1, 0, LANG_NEUTRAL, &pTypeLib)))	return E_FAIL;
	HRESULT hr = pTypeLib->GetTypeInfoOfGuid(IID_IDouble, &pMiniDcom->m_pTypeInfo);
	pTypeLib->Release();
	if(FAILED(hr)) return hr;
	hr = pMiniDcom->QueryInterface(iid, ppv);
	pMiniDcom->Release();
	return hr;
}

HRESULT CFactory::LockServer(BOOL bLock) 
{
	if(bLock) CoAddRefServerProcess();
	else if(CoReleaseServerProcess() == 0)
			SetEvent(g_hEvent);
	return S_OK;
}

HRESULT RegisterServer(const char* szModuleName, REFCLSID clsid, const char* szFriendlyName,
   const char* szVerIndProgID, const char* szProgID, const char* szThreadingModel);
LONG UnregisterServer(REFCLSID clsid, const char* szVerIndProgID, const char* szProgID);

void main(int argc, char** argv)
{
	ITypeLib* pTypeLib;
	HRESULT hr = LoadTypeLibEx(L"Server.exe", REGKIND_DEFAULT, &pTypeLib);
	pTypeLib->Release();
	RegisterServer("Server.exe", CLSID_MiniDcom, "MiniDcom Sample", "Component.MiniDcom", "Component.MiniDcom.1", NULL);
	if(argc < 2)
	{
		cout << "Server Regestered." << endl;
		exit(true);
	}
	char* szToken = strtok(argv[1], "-/");
	if(_stricmp(szToken, "u") == 0)
	{
		UnRegisterTypeLib(LIBID_Server, 1, 0, LANG_NEUTRAL, SYS_WIN32);
		UnregisterServer(CLSID_MiniDcom, "Component.MiniDcom", "Component.MiniDcom.1");
		cout << "Server Unregestered." << endl;
		exit(true);
	}
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	g_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	DWORD dwRegister;
	IClassFactory *pIFactory = new CFactory();
	CoRegisterClassObject(CLSID_MiniDcom, pIFactory, CLSCTX_LOCAL_SERVER, REGCLS_SUSPENDED|REGCLS_MULTIPLEUSE, &dwRegister);
	cout << "Out-of-process Dual-interface DCOM Server Started." << endl;
	CoResumeClassObjects(); 
	WaitForSingleObject(g_hEvent, INFINITE);
	CloseHandle(g_hEvent);
	CoRevokeClassObject(dwRegister);
	pIFactory->Release();
	CoUninitialize();
	cout << "Server Exited." << endl;
}

#include <objbase.h>
BOOL AddKey(const char* pszPath, const char* szSubkey, const char* szValue);
BOOL SetKey(const char* szKey, const char* szNamedValue, const char* szValue);
void GuidToStr(REFCLSID clsid, char* szCLSID, int length);
LONG DeleteKey(HKEY hKeyParent, const char* szKeyChild);

HRESULT RegisterServer(const char* szModuleName, REFCLSID clsid, const char* szFriendlyName,
   const char* szVerIndProgID, const char* szProgID, const char* szThreadingModel)
{
	char szModule[512];
	HMODULE hModule = GetModuleHandle(szModuleName);
	DWORD dwResult = GetModuleFileName(hModule, szModule, sizeof(szModule)/sizeof(char));
	char szCLSID[39];
	GuidToStr(clsid, szCLSID, sizeof(szCLSID));
	char szKey[64];
	strcpy(szKey, "CLSID\\");
	strcat(szKey, szCLSID);
	AddKey(szKey, NULL, szFriendlyName);
	if(strstr(szModuleName, ".exe") == NULL)
	{
		AddKey(szKey, "InprocServer32", szModule);
		char szInproc[64];
		strcpy(szInproc, szKey);
		strcat(szInproc, "\\InprocServer32");
		SetKey(szInproc, "ThreadingModel", szThreadingModel);
	}
	else
		AddKey(szKey, "LocalServer32", szModule);
	AddKey(szKey, "ProgID", szProgID);
	AddKey(szKey, "VersionIndependentProgID", szVerIndProgID);
	AddKey(szVerIndProgID, NULL, szFriendlyName);
	AddKey(szVerIndProgID, "CLSID", szCLSID);
	AddKey(szVerIndProgID, "CurVer", szProgID);
	AddKey(szProgID, NULL, szFriendlyName);
	AddKey(szProgID, "CLSID", szCLSID);
	return S_OK;
}

LONG UnregisterServer(REFCLSID clsid, const char* szVerIndProgID, const char* szProgID)
{
	char szCLSID[39];
	GuidToStr(clsid, szCLSID, sizeof(szCLSID));
	char szKey[64];
	strcpy(szKey, "CLSID\\");
	strcat(szKey, szCLSID);
	LONG lResult = DeleteKey(HKEY_CLASSES_ROOT, szKey);
	lResult = DeleteKey(HKEY_CLASSES_ROOT, szVerIndProgID);
	lResult = DeleteKey(HKEY_CLASSES_ROOT, szProgID);
	return S_OK;
}

void GuidToStr(REFCLSID clsid, char* szCLSID, int length)
{
	LPOLESTR wszCLSID = NULL;
	HRESULT hr = StringFromCLSID(clsid, &wszCLSID);
	wcstombs(szCLSID, wszCLSID, length);
	CoTaskMemFree(wszCLSID);
}

LONG DeleteKey(HKEY hKeyParent, const char* lpszKeyChild)
{
	HKEY hKeyChild;
	LONG lRes = RegOpenKeyEx(hKeyParent, lpszKeyChild, 0, KEY_ALL_ACCESS, &hKeyChild);
	if(lRes != ERROR_SUCCESS) return lRes;
	FILETIME time;
	char szBuffer[256];
	DWORD dwSize = 256;
	while(RegEnumKeyEx(hKeyChild, 0, szBuffer, &dwSize, NULL, NULL, NULL, &time) == S_OK)
	{
		lRes = DeleteKey(hKeyChild, szBuffer);
		if(lRes != ERROR_SUCCESS)
		{
			RegCloseKey(hKeyChild);
			return lRes;
		}
		dwSize = 256;
	}
	RegCloseKey(hKeyChild);
	return RegDeleteKey(hKeyParent, lpszKeyChild);
}

BOOL AddKey(const char* szKey, const char* szSubkey, const char* szValue)
{
	HKEY hKey;
	char szKeyBuf[1024];
	strcpy(szKeyBuf, szKey);
	if(szSubkey != NULL)
	{
		strcat(szKeyBuf, "\\");
		strcat(szKeyBuf, szSubkey );
	}
	long lResult = RegCreateKeyEx(HKEY_CLASSES_ROOT, szKeyBuf, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if(lResult != ERROR_SUCCESS) return FALSE;
	if(szValue != NULL)
		RegSetValueEx(hKey, NULL, 0, REG_SZ, (BYTE *)szValue, strlen(szValue)+1);
	RegCloseKey(hKey);
	return TRUE;
}

BOOL SetKey(const char* szKey, const char* szNamedValue, const char* szValue)
{
	HKEY hKey;
	char szKeyBuf[1024];
	strcpy(szKeyBuf, szKey);
	long lResult = RegOpenKeyEx(HKEY_CLASSES_ROOT, szKeyBuf, 0, KEY_SET_VALUE, &hKey);
	if(lResult != ERROR_SUCCESS) return FALSE;
	if(szValue != NULL)
		RegSetValueEx(hKey, szNamedValue, 0, REG_SZ, (BYTE*)szValue, strlen(szValue)+1);
	RegCloseKey(hKey);
	return TRUE;
}