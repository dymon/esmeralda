

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Fri Oct 30 05:02:04 2015
 */
/* Compiler settings for Server.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __Server_h__
#define __Server_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDouble_FWD_DEFINED__
#define __IDouble_FWD_DEFINED__
typedef interface IDouble IDouble;
#endif 	/* __IDouble_FWD_DEFINED__ */


#ifndef __IDouble_FWD_DEFINED__
#define __IDouble_FWD_DEFINED__
typedef interface IDouble IDouble;
#endif 	/* __IDouble_FWD_DEFINED__ */


#ifndef __MiniDcom_FWD_DEFINED__
#define __MiniDcom_FWD_DEFINED__

#ifdef __cplusplus
typedef class MiniDcom MiniDcom;
#else
typedef struct MiniDcom MiniDcom;
#endif /* __cplusplus */

#endif 	/* __MiniDcom_FWD_DEFINED__ */


/* header files for imported files */
#include "unknwn.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IDouble_INTERFACE_DEFINED__
#define __IDouble_INTERFACE_DEFINED__

/* interface IDouble */
/* [dual][uuid][object] */ 


EXTERN_C const IID IID_IDouble;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("10000001-0000-0000-0000-000000000001")
    IDouble : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Double( 
            int x,
            /* [retval][out] */ int *retval) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDoubleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDouble * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDouble * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDouble * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDouble * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDouble * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDouble * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDouble * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Double )( 
            IDouble * This,
            int x,
            /* [retval][out] */ int *retval);
        
        END_INTERFACE
    } IDoubleVtbl;

    interface IDouble
    {
        CONST_VTBL struct IDoubleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDouble_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDouble_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDouble_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDouble_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IDouble_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IDouble_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IDouble_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IDouble_Double(This,x,retval)	\
    ( (This)->lpVtbl -> Double(This,x,retval) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDouble_INTERFACE_DEFINED__ */



#ifndef __Server_LIBRARY_DEFINED__
#define __Server_LIBRARY_DEFINED__

/* library Server */
/* [version][helpstring][uuid] */ 



EXTERN_C const IID LIBID_Server;

EXTERN_C const CLSID CLSID_MiniDcom;

#ifdef __cplusplus

class DECLSPEC_UUID("10000002-0000-0000-0000-000000000001")
MiniDcom;
#endif
#endif /* __Server_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


