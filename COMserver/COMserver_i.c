

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed Oct 28 18:06:00 2015
 */
/* Compiler settings for COMserver.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_ISHMEMserverCallback,0x5ACF8D95,0x47FE,0x405D,0x82,0xBE,0x3B,0x9F,0x07,0xD3,0xB1,0x4A);


MIDL_DEFINE_GUID(IID, IID_ISHMEMid,0xEB6E78BC,0x5CA8,0x4B20,0x89,0x79,0x92,0x0D,0x16,0x2B,0xB5,0x88);


MIDL_DEFINE_GUID(IID, IID_ISHMEMserver,0xA1514FBB,0x5F8D,0x44C5,0xA4,0x55,0xE1,0x41,0xD5,0xCA,0x23,0x73);


MIDL_DEFINE_GUID(IID, IID_ISHMEMclient,0x00C1FA1D,0xC4FD,0x4449,0x9E,0x1A,0x62,0xAA,0x6C,0x51,0x02,0x7A);


MIDL_DEFINE_GUID(IID, LIBID_COMserverLib,0x8400881B,0x95DB,0x42CA,0x9D,0x4E,0x27,0x68,0xFC,0xD5,0x12,0x73);


MIDL_DEFINE_GUID(CLSID, CLSID_SHMEMcommunicator,0xA15897F3,0x8AAA,0x4F41,0xB4,0xEF,0xB1,0x06,0xDC,0xCF,0xB0,0xCD);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



