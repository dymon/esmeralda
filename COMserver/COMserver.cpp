// COMserver.cpp: ���������� WinMain


#include "stdafx.h"
#include "resource.h"
#include "COMserver_i.h"
#include "xdlldata.h"



class CCOMserverModule : public ATL::CAtlExeModuleT< CCOMserverModule >
	{
public :
	DECLARE_LIBID(LIBID_COMserverLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_COMSERVER, "{1153CBE6-AB30-4B29-B11C-66799B704D09}")
	};

CCOMserverModule _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	return _AtlModule.WinMain(nShowCmd);
}

