

/* this ALWAYS GENERATED file contains the proxy stub code */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed Oct 28 18:06:00 2015
 */
/* Compiler settings for COMserver.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#if !defined(_M_IA64) && !defined(_M_AMD64)


#pragma warning( disable: 4049 )  /* more than 64k source lines */
#if _MSC_VER >= 1200
#pragma warning(push)
#endif

#pragma warning( disable: 4211 )  /* redefine extern to static */
#pragma warning( disable: 4232 )  /* dllimport identity*/
#pragma warning( disable: 4024 )  /* array to pointer mapping*/
#pragma warning( disable: 4152 )  /* function/data pointer conversion in expression */
#pragma warning( disable: 4100 ) /* unreferenced arguments in x86 call */

#pragma optimize("", off ) 

#define USE_STUBLESS_PROXY


/* verify that the <rpcproxy.h> version is high enough to compile this file*/
#ifndef __REDQ_RPCPROXY_H_VERSION__
#define __REQUIRED_RPCPROXY_H_VERSION__ 475
#endif


#include "rpcproxy.h"
#ifndef __RPCPROXY_H_VERSION__
#error this stub requires an updated version of <rpcproxy.h>
#endif /* __RPCPROXY_H_VERSION__ */


#include "COMserver_i.h"

#define TYPE_FORMAT_STRING_SIZE   29                                
#define PROC_FORMAT_STRING_SIZE   163                               
#define EXPR_FORMAT_STRING_SIZE   1                                 
#define TRANSMIT_AS_TABLE_SIZE    0            
#define WIRE_MARSHAL_TABLE_SIZE   0            

typedef struct _COMserver_MIDL_TYPE_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ TYPE_FORMAT_STRING_SIZE ];
    } COMserver_MIDL_TYPE_FORMAT_STRING;

typedef struct _COMserver_MIDL_PROC_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ PROC_FORMAT_STRING_SIZE ];
    } COMserver_MIDL_PROC_FORMAT_STRING;

typedef struct _COMserver_MIDL_EXPR_FORMAT_STRING
    {
    long          Pad;
    unsigned char  Format[ EXPR_FORMAT_STRING_SIZE ];
    } COMserver_MIDL_EXPR_FORMAT_STRING;


static const RPC_SYNTAX_IDENTIFIER  _RpcTransferSyntax = 
{{0x8A885D04,0x1CEB,0x11C9,{0x9F,0xE8,0x08,0x00,0x2B,0x10,0x48,0x60}},{2,0}};


extern const COMserver_MIDL_TYPE_FORMAT_STRING COMserver__MIDL_TypeFormatString;
extern const COMserver_MIDL_PROC_FORMAT_STRING COMserver__MIDL_ProcFormatString;
extern const COMserver_MIDL_EXPR_FORMAT_STRING COMserver__MIDL_ExprFormatString;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO ISHMEMserverCallback_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO ISHMEMserverCallback_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO ISHMEMserver_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO ISHMEMserver_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO ISHMEMclient_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO ISHMEMclient_ProxyInfo;



#if !defined(__RPC_WIN32__)
#error  Invalid build platform for this stub.
#endif

#if !(TARGET_IS_NT50_OR_LATER)
#error You need Windows 2000 or later to run this stub because it uses these features:
#error   /robust command line switch.
#error However, your C/C++ compilation flags indicate you intend to run this app on earlier systems.
#error This app will fail with the RPC_X_WRONG_STUB_VERSION error.
#endif


static const COMserver_MIDL_PROC_FORMAT_STRING COMserver__MIDL_ProcFormatString =
    {
        0,
        {

	/* Procedure TransferStarted */

			0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/*  2 */	NdrFcLong( 0x0 ),	/* 0 */
/*  6 */	NdrFcShort( 0x3 ),	/* 3 */
/*  8 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 10 */	NdrFcShort( 0x8 ),	/* 8 */
/* 12 */	NdrFcShort( 0x8 ),	/* 8 */
/* 14 */	0x46,		/* Oi2 Flags:  clt must size, has return, has ext, */
			0x3,		/* 3 */
/* 16 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 18 */	NdrFcShort( 0x0 ),	/* 0 */
/* 20 */	NdrFcShort( 0x0 ),	/* 0 */
/* 22 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter dwClientID */

/* 24 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 26 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 28 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter szFileName */

/* 30 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 32 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 34 */	NdrFcShort( 0x4 ),	/* Type Offset=4 */

	/* Return value */

/* 36 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 38 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 40 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure InitializeServer */

/* 42 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 44 */	NdrFcLong( 0x0 ),	/* 0 */
/* 48 */	NdrFcShort( 0x4 ),	/* 4 */
/* 50 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 52 */	NdrFcShort( 0x0 ),	/* 0 */
/* 54 */	NdrFcShort( 0x8 ),	/* 8 */
/* 56 */	0x46,		/* Oi2 Flags:  clt must size, has return, has ext, */
			0x2,		/* 2 */
/* 58 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 60 */	NdrFcShort( 0x0 ),	/* 0 */
/* 62 */	NdrFcShort( 0x0 ),	/* 0 */
/* 64 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter pUnk */

/* 66 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 68 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 70 */	NdrFcShort( 0x6 ),	/* Type Offset=6 */

	/* Return value */

/* 72 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 74 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 76 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure InitializeClient */

/* 78 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 80 */	NdrFcLong( 0x0 ),	/* 0 */
/* 84 */	NdrFcShort( 0x4 ),	/* 4 */
/* 86 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 88 */	NdrFcShort( 0x8 ),	/* 8 */
/* 90 */	NdrFcShort( 0x24 ),	/* 36 */
/* 92 */	0x44,		/* Oi2 Flags:  has return, has ext, */
			0x3,		/* 3 */
/* 94 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 96 */	NdrFcShort( 0x0 ),	/* 0 */
/* 98 */	NdrFcShort( 0x0 ),	/* 0 */
/* 100 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter dwChunkSize */

/* 102 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 104 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 106 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter pdwClientID */

/* 108 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 110 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 112 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Return value */

/* 114 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 116 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 118 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure EnqueueFile */

/* 120 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 122 */	NdrFcLong( 0x0 ),	/* 0 */
/* 126 */	NdrFcShort( 0x5 ),	/* 5 */
/* 128 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 130 */	NdrFcShort( 0x8 ),	/* 8 */
/* 132 */	NdrFcShort( 0x8 ),	/* 8 */
/* 134 */	0x46,		/* Oi2 Flags:  clt must size, has return, has ext, */
			0x3,		/* 3 */
/* 136 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 138 */	NdrFcShort( 0x0 ),	/* 0 */
/* 140 */	NdrFcShort( 0x0 ),	/* 0 */
/* 142 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter dwClientID */

/* 144 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 146 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 148 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter szFileName */

/* 150 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 152 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 154 */	NdrFcShort( 0x4 ),	/* Type Offset=4 */

	/* Return value */

/* 156 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 158 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 160 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

			0x0
        }
    };

static const COMserver_MIDL_TYPE_FORMAT_STRING COMserver__MIDL_TypeFormatString =
    {
        0,
        {
			NdrFcShort( 0x0 ),	/* 0 */
/*  2 */	
			0x11, 0x8,	/* FC_RP [simple_pointer] */
/*  4 */	
			0x25,		/* FC_C_WSTRING */
			0x5c,		/* FC_PAD */
/*  6 */	
			0x2f,		/* FC_IP */
			0x5a,		/* FC_CONSTANT_IID */
/*  8 */	NdrFcLong( 0x5acf8d95 ),	/* 1523551637 */
/* 12 */	NdrFcShort( 0x47fe ),	/* 18430 */
/* 14 */	NdrFcShort( 0x405d ),	/* 16477 */
/* 16 */	0x82,		/* 130 */
			0xbe,		/* 190 */
/* 18 */	0x3b,		/* 59 */
			0x9f,		/* 159 */
/* 20 */	0x7,		/* 7 */
			0xd3,		/* 211 */
/* 22 */	0xb1,		/* 177 */
			0x4a,		/* 74 */
/* 24 */	
			0x11, 0xc,	/* FC_RP [alloced_on_stack] [simple_pointer] */
/* 26 */	0x8,		/* FC_LONG */
			0x5c,		/* FC_PAD */

			0x0
        }
    };


/* Object interface: IUnknown, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */


/* Object interface: ISHMEMserverCallback, ver. 0.0,
   GUID={0x5ACF8D95,0x47FE,0x405D,{0x82,0xBE,0x3B,0x9F,0x07,0xD3,0xB1,0x4A}} */

#pragma code_seg(".orpc")
static const unsigned short ISHMEMserverCallback_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO ISHMEMserverCallback_ProxyInfo =
    {
    &Object_StubDesc,
    COMserver__MIDL_ProcFormatString.Format,
    &ISHMEMserverCallback_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO ISHMEMserverCallback_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    COMserver__MIDL_ProcFormatString.Format,
    &ISHMEMserverCallback_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(4) _ISHMEMserverCallbackProxyVtbl = 
{
    &ISHMEMserverCallback_ProxyInfo,
    &IID_ISHMEMserverCallback,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    (void *) (INT_PTR) -1 /* ISHMEMserverCallback::TransferStarted */
};

const CInterfaceStubVtbl _ISHMEMserverCallbackStubVtbl =
{
    &IID_ISHMEMserverCallback,
    &ISHMEMserverCallback_ServerInfo,
    4,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: ISHMEMid, ver. 0.0,
   GUID={0xEB6E78BC,0x5CA8,0x4B20,{0x89,0x79,0x92,0x0D,0x16,0x2B,0xB5,0x88}} */


/* Object interface: ISHMEMserver, ver. 0.0,
   GUID={0xA1514FBB,0x5F8D,0x44C5,{0xA4,0x55,0xE1,0x41,0xD5,0xCA,0x23,0x73}} */

#pragma code_seg(".orpc")
static const unsigned short ISHMEMserver_FormatStringOffsetTable[] =
    {
    (unsigned short) -1,
    42
    };

static const MIDL_STUBLESS_PROXY_INFO ISHMEMserver_ProxyInfo =
    {
    &Object_StubDesc,
    COMserver__MIDL_ProcFormatString.Format,
    &ISHMEMserver_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO ISHMEMserver_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    COMserver__MIDL_ProcFormatString.Format,
    &ISHMEMserver_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(5) _ISHMEMserverProxyVtbl = 
{
    &ISHMEMserver_ProxyInfo,
    &IID_ISHMEMserver,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    0 /* ISHMEMid::GetSharedMemoryName */ ,
    (void *) (INT_PTR) -1 /* ISHMEMserver::InitializeServer */
};


static const PRPC_STUB_FUNCTION ISHMEMserver_table[] =
{
    STUB_FORWARDING_FUNCTION,
    NdrStubCall2
};

CInterfaceStubVtbl _ISHMEMserverStubVtbl =
{
    &IID_ISHMEMserver,
    &ISHMEMserver_ServerInfo,
    5,
    &ISHMEMserver_table[-3],
    CStdStubBuffer_DELEGATING_METHODS
};


/* Object interface: ISHMEMclient, ver. 0.0,
   GUID={0x00C1FA1D,0xC4FD,0x4449,{0x9E,0x1A,0x62,0xAA,0x6C,0x51,0x02,0x7A}} */

#pragma code_seg(".orpc")
static const unsigned short ISHMEMclient_FormatStringOffsetTable[] =
    {
    (unsigned short) -1,
    78,
    120
    };

static const MIDL_STUBLESS_PROXY_INFO ISHMEMclient_ProxyInfo =
    {
    &Object_StubDesc,
    COMserver__MIDL_ProcFormatString.Format,
    &ISHMEMclient_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO ISHMEMclient_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    COMserver__MIDL_ProcFormatString.Format,
    &ISHMEMclient_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(6) _ISHMEMclientProxyVtbl = 
{
    &ISHMEMclient_ProxyInfo,
    &IID_ISHMEMclient,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    0 /* ISHMEMid::GetSharedMemoryName */ ,
    (void *) (INT_PTR) -1 /* ISHMEMclient::InitializeClient */ ,
    (void *) (INT_PTR) -1 /* ISHMEMclient::EnqueueFile */
};


static const PRPC_STUB_FUNCTION ISHMEMclient_table[] =
{
    STUB_FORWARDING_FUNCTION,
    NdrStubCall2,
    NdrStubCall2
};

CInterfaceStubVtbl _ISHMEMclientStubVtbl =
{
    &IID_ISHMEMclient,
    &ISHMEMclient_ServerInfo,
    6,
    &ISHMEMclient_table[-3],
    CStdStubBuffer_DELEGATING_METHODS
};

static const MIDL_STUB_DESC Object_StubDesc = 
    {
    0,
    NdrOleAllocate,
    NdrOleFree,
    0,
    0,
    0,
    0,
    0,
    COMserver__MIDL_TypeFormatString.Format,
    1, /* -error bounds_check flag */
    0x50002, /* Ndr library version */
    0,
    0x700022b, /* MIDL Version 7.0.555 */
    0,
    0,
    0,  /* notify & notify_flag routine table */
    0x1, /* MIDL flag */
    0, /* cs routines */
    0,   /* proxy/server info */
    0
    };

const CInterfaceProxyVtbl * const _COMserver_ProxyVtblList[] = 
{
    ( CInterfaceProxyVtbl *) &_ISHMEMclientProxyVtbl,
    ( CInterfaceProxyVtbl *) &_ISHMEMserverCallbackProxyVtbl,
    ( CInterfaceProxyVtbl *) &_ISHMEMserverProxyVtbl,
    0
};

const CInterfaceStubVtbl * const _COMserver_StubVtblList[] = 
{
    ( CInterfaceStubVtbl *) &_ISHMEMclientStubVtbl,
    ( CInterfaceStubVtbl *) &_ISHMEMserverCallbackStubVtbl,
    ( CInterfaceStubVtbl *) &_ISHMEMserverStubVtbl,
    0
};

PCInterfaceName const _COMserver_InterfaceNamesList[] = 
{
    "ISHMEMclient",
    "ISHMEMserverCallback",
    "ISHMEMserver",
    0
};

const IID *  const _COMserver_BaseIIDList[] = 
{
    &IID_ISHMEMid,
    0,
    &IID_ISHMEMid,
    0
};


#define _COMserver_CHECK_IID(n)	IID_GENERIC_CHECK_IID( _COMserver, pIID, n)

int __stdcall _COMserver_IID_Lookup( const IID * pIID, int * pIndex )
{
    IID_BS_LOOKUP_SETUP

    IID_BS_LOOKUP_INITIAL_TEST( _COMserver, 3, 2 )
    IID_BS_LOOKUP_NEXT_TEST( _COMserver, 1 )
    IID_BS_LOOKUP_RETURN_RESULT( _COMserver, 3, *pIndex )
    
}

const ExtendedProxyFileInfo COMserver_ProxyFileInfo = 
{
    (PCInterfaceProxyVtblList *) & _COMserver_ProxyVtblList,
    (PCInterfaceStubVtblList *) & _COMserver_StubVtblList,
    (const PCInterfaceName * ) & _COMserver_InterfaceNamesList,
    (const IID ** ) & _COMserver_BaseIIDList,
    & _COMserver_IID_Lookup, 
    3,
    2,
    0, /* table of [async_uuid] interfaces */
    0, /* Filler1 */
    0, /* Filler2 */
    0  /* Filler3 */
};
#pragma optimize("", on )
#if _MSC_VER >= 1200
#pragma warning(pop)
#endif


#endif /* !defined(_M_IA64) && !defined(_M_AMD64)*/

