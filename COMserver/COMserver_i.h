

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed Oct 28 18:06:00 2015
 */
/* Compiler settings for COMserver.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __COMserver_i_h__
#define __COMserver_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ISHMEMserverCallback_FWD_DEFINED__
#define __ISHMEMserverCallback_FWD_DEFINED__
typedef interface ISHMEMserverCallback ISHMEMserverCallback;
#endif 	/* __ISHMEMserverCallback_FWD_DEFINED__ */


#ifndef __ISHMEMid_FWD_DEFINED__
#define __ISHMEMid_FWD_DEFINED__
typedef interface ISHMEMid ISHMEMid;
#endif 	/* __ISHMEMid_FWD_DEFINED__ */


#ifndef __ISHMEMserver_FWD_DEFINED__
#define __ISHMEMserver_FWD_DEFINED__
typedef interface ISHMEMserver ISHMEMserver;
#endif 	/* __ISHMEMserver_FWD_DEFINED__ */


#ifndef __ISHMEMclient_FWD_DEFINED__
#define __ISHMEMclient_FWD_DEFINED__
typedef interface ISHMEMclient ISHMEMclient;
#endif 	/* __ISHMEMclient_FWD_DEFINED__ */


#ifndef __SHMEMcommunicator_FWD_DEFINED__
#define __SHMEMcommunicator_FWD_DEFINED__

#ifdef __cplusplus
typedef class SHMEMcommunicator SHMEMcommunicator;
#else
typedef struct SHMEMcommunicator SHMEMcommunicator;
#endif /* __cplusplus */

#endif 	/* __SHMEMcommunicator_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ISHMEMserverCallback_INTERFACE_DEFINED__
#define __ISHMEMserverCallback_INTERFACE_DEFINED__

/* interface ISHMEMserverCallback */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_ISHMEMserverCallback;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5ACF8D95-47FE-405D-82BE-3B9F07D3B14A")
    ISHMEMserverCallback : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE TransferStarted( 
            /* [in] */ DWORD dwClientID,
            /* [string][in] */ LPCWSTR szFileName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISHMEMserverCallbackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISHMEMserverCallback * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISHMEMserverCallback * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISHMEMserverCallback * This);
        
        HRESULT ( STDMETHODCALLTYPE *TransferStarted )( 
            ISHMEMserverCallback * This,
            /* [in] */ DWORD dwClientID,
            /* [string][in] */ LPCWSTR szFileName);
        
        END_INTERFACE
    } ISHMEMserverCallbackVtbl;

    interface ISHMEMserverCallback
    {
        CONST_VTBL struct ISHMEMserverCallbackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISHMEMserverCallback_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISHMEMserverCallback_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISHMEMserverCallback_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISHMEMserverCallback_TransferStarted(This,dwClientID,szFileName)	\
    ( (This)->lpVtbl -> TransferStarted(This,dwClientID,szFileName) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISHMEMserverCallback_INTERFACE_DEFINED__ */


#ifndef __ISHMEMid_INTERFACE_DEFINED__
#define __ISHMEMid_INTERFACE_DEFINED__

/* interface ISHMEMid */
/* [unique][uuid][local][object] */ 


EXTERN_C const IID IID_ISHMEMid;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EB6E78BC-5CA8-4B20-8979-920D162BB588")
    ISHMEMid : public IUnknown
    {
    public:
        virtual LPWSTR STDMETHODCALLTYPE GetSharedMemoryName( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISHMEMidVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISHMEMid * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISHMEMid * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISHMEMid * This);
        
        LPWSTR ( STDMETHODCALLTYPE *GetSharedMemoryName )( 
            ISHMEMid * This);
        
        END_INTERFACE
    } ISHMEMidVtbl;

    interface ISHMEMid
    {
        CONST_VTBL struct ISHMEMidVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISHMEMid_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISHMEMid_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISHMEMid_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISHMEMid_GetSharedMemoryName(This)	\
    ( (This)->lpVtbl -> GetSharedMemoryName(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISHMEMid_INTERFACE_DEFINED__ */


#ifndef __ISHMEMserver_INTERFACE_DEFINED__
#define __ISHMEMserver_INTERFACE_DEFINED__

/* interface ISHMEMserver */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_ISHMEMserver;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A1514FBB-5F8D-44C5-A455-E141D5CA2373")
    ISHMEMserver : public ISHMEMid
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE InitializeServer( 
            ISHMEMserverCallback *pUnk) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISHMEMserverVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISHMEMserver * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISHMEMserver * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISHMEMserver * This);
        
        LPWSTR ( STDMETHODCALLTYPE *GetSharedMemoryName )( 
            ISHMEMserver * This);
        
        HRESULT ( STDMETHODCALLTYPE *InitializeServer )( 
            ISHMEMserver * This,
            ISHMEMserverCallback *pUnk);
        
        END_INTERFACE
    } ISHMEMserverVtbl;

    interface ISHMEMserver
    {
        CONST_VTBL struct ISHMEMserverVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISHMEMserver_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISHMEMserver_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISHMEMserver_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISHMEMserver_GetSharedMemoryName(This)	\
    ( (This)->lpVtbl -> GetSharedMemoryName(This) ) 


#define ISHMEMserver_InitializeServer(This,pUnk)	\
    ( (This)->lpVtbl -> InitializeServer(This,pUnk) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISHMEMserver_INTERFACE_DEFINED__ */


#ifndef __ISHMEMclient_INTERFACE_DEFINED__
#define __ISHMEMclient_INTERFACE_DEFINED__

/* interface ISHMEMclient */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_ISHMEMclient;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("00C1FA1D-C4FD-4449-9E1A-62AA6C51027A")
    ISHMEMclient : public ISHMEMid
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE InitializeClient( 
            /* [in] */ DWORD dwChunkSize,
            /* [out] */ DWORD *pdwClientID) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE EnqueueFile( 
            /* [in] */ DWORD dwClientID,
            /* [string][in] */ LPCWSTR szFileName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISHMEMclientVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISHMEMclient * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISHMEMclient * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISHMEMclient * This);
        
        LPWSTR ( STDMETHODCALLTYPE *GetSharedMemoryName )( 
            ISHMEMclient * This);
        
        HRESULT ( STDMETHODCALLTYPE *InitializeClient )( 
            ISHMEMclient * This,
            /* [in] */ DWORD dwChunkSize,
            /* [out] */ DWORD *pdwClientID);
        
        HRESULT ( STDMETHODCALLTYPE *EnqueueFile )( 
            ISHMEMclient * This,
            /* [in] */ DWORD dwClientID,
            /* [string][in] */ LPCWSTR szFileName);
        
        END_INTERFACE
    } ISHMEMclientVtbl;

    interface ISHMEMclient
    {
        CONST_VTBL struct ISHMEMclientVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISHMEMclient_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISHMEMclient_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISHMEMclient_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISHMEMclient_GetSharedMemoryName(This)	\
    ( (This)->lpVtbl -> GetSharedMemoryName(This) ) 


#define ISHMEMclient_InitializeClient(This,dwChunkSize,pdwClientID)	\
    ( (This)->lpVtbl -> InitializeClient(This,dwChunkSize,pdwClientID) ) 

#define ISHMEMclient_EnqueueFile(This,dwClientID,szFileName)	\
    ( (This)->lpVtbl -> EnqueueFile(This,dwClientID,szFileName) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISHMEMclient_INTERFACE_DEFINED__ */



#ifndef __COMserverLib_LIBRARY_DEFINED__
#define __COMserverLib_LIBRARY_DEFINED__

/* library COMserverLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_COMserverLib;

EXTERN_C const CLSID CLSID_SHMEMcommunicator;

#ifdef __cplusplus

class DECLSPEC_UUID("A15897F3-8AAA-4F41-B4EF-B106DCCFB0CD")
SHMEMcommunicator;
#endif
#endif /* __COMserverLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


