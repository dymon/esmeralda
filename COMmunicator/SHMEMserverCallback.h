#pragma once

#include "COMmunicator_h.h"
class CSHMEMserverCallback : public ISHMEMserverCallback
{
public:
	CSHMEMserverCallback(void);
	virtual ~CSHMEMserverCallback(void);

	STDMETHOD(TransferStarted)(
	    DWORD   dwClientID,
	    LPCWSTR szFileName,
	    DWORD   dwChunkSize,
		DWORD   dwChunkCount,
		DWORD   dwBytesInLastChunk
	);
};

