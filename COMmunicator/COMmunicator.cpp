// COMmunicator.cpp by Dmitry Obuvalin October 2015
// Based on Server.cpp by Walter <wxc@email.com> September 2001 -- special thanks
#include "COMmunicatorClass.h"
HRESULT COMmunicator::GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo** ppITypeInfo)
{
	*ppITypeInfo = NULL;
	m_pTypeInfo->AddRef();
	*ppITypeInfo = m_pTypeInfo;
	return S_OK;
}

ULONG COMmunicator::Release()
{
	unsigned cRef = InterlockedDecrement(&m_cRef);
	if(cRef != 0) return cRef;
	m_pTypeInfo->Release(); 
	delete this;
	return 0;
}

HRESULT COMmunicator::QueryInterface(REFIID iid, void **ppv)
{
	HRESULT hr = S_OK;
	*ppv = 0;
	if (iid == IID_IUnknown) {
		*ppv = (this);
	}
	else if (iid == IID_ISHMEMid) {
		ISHMEMserver *p1 = (ISHMEMserver*)this;
		*ppv = (ISHMEMid*)p1;
	}
	else if (iid == IID_ISHMEMserver) {
		*ppv = (ISHMEMserver*)this;
	}
	else if (iid == IID_ISHMEMclient) {
		*ppv = (ISHMEMclient*)this; 
	}
	else {
		hr = E_NOINTERFACE;
	}
	if (*ppv) AddRef();
	return hr;
}

// Test Task Specific functions
STDMETHODIMP_(LPWSTR) COMmunicator::GetSharedMemoryName(void) {
	return L"suka";
}
STDMETHODIMP COMmunicator::InitializeServer(ISHMEMserverCallback *pUnk) {
	return E_NOTIMPL;
}

STDMETHODIMP COMmunicator::FinalizeServer() {
	return E_NOTIMPL;
}

STDMETHODIMP COMmunicator::InitializeClient(
	DWORD   dwChunkSize,
	DWORD * pdwClientID
) {
	return E_NOTIMPL;
}

STDMETHODIMP COMmunicator::EnqueueFile(
	DWORD   dwClientID,
	LPCWSTR szFileName
) {
	return E_NOTIMPL;
}

STDMETHODIMP COMmunicator::FinalizeClient(
	DWORD   dwClientID
) {
	return E_NOTIMPL;
}

// End of Testing Task Specific

class CFactory : public IClassFactory
{
public:
	ULONG __stdcall AddRef() {return InterlockedIncrement(&m_cRef);};;
	ULONG __stdcall Release();
	HRESULT __stdcall QueryInterface(REFIID iid, void** ppv);
	HRESULT __stdcall CreateInstance(IUnknown *pUnknownOuter, REFIID iid, void** ppv);
	HRESULT __stdcall LockServer(BOOL bLock);
	CFactory() : m_cRef(1) { }
	~CFactory() {  }
private:
	long m_cRef;
};


ULONG CFactory::Release()
{
	unsigned cRef = InterlockedDecrement(&m_cRef);
	if(cRef != 0) return cRef;
	delete this;
	return 0;
}

HRESULT CFactory::QueryInterface(REFIID iid, void** ppv)
{
	if((iid == IID_IUnknown) || (iid == IID_IClassFactory))
		*ppv = (IClassFactory *)this;
	else 
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}
	AddRef();
	return S_OK;
}

HRESULT CFactory::CreateInstance(IUnknown *pUnknownOuter, REFIID iid, void **ppv)
{
	if(pUnknownOuter != NULL)	return CLASS_E_NOAGGREGATION;
	COMmunicator *pCOMcom = new COMmunicator; 
	if(pCOMcom == NULL) return E_OUTOFMEMORY;
	//ITypeLib* pTypeLib;
	//if(FAILED(LoadRegTypeLib(LIBID_COMmunicatorLib, 1, 0, LANG_NEUTRAL, &pTypeLib)))	return E_FAIL;
	//HRESULT hr = pTypeLib->GetTypeInfoOfGuid(IID_IDouble, &pCOMcom->m_pTypeInfo);
	//pTypeLib->Release();
	//if(FAILED(hr)) return hr;
	HRESULT hr = pCOMcom->QueryInterface(iid, ppv);
	pCOMcom->Release();
	return hr;
}

HRESULT CFactory::LockServer(BOOL bLock) 
{
	if(bLock) CoAddRefServerProcess();
	else if(CoReleaseServerProcess() == 0)
			SetEvent(g_hEvent);
	return S_OK;
}

HRESULT RegisterServer(const char* szModuleName, REFCLSID clsid, const char* szFriendlyName,
   const char* szVerIndProgID, const char* szProgID, const char* szThreadingModel);
LONG UnregisterServer(REFCLSID clsid, const char* szVerIndProgID, const char* szProgID);

#define SERVER_PROGID "Esmeralda.COMcom"
#define SERVER_VERS ".1"

void main(int argc, char** argv)
{
	ITypeLib* pTypeLib;
	HRESULT hr = LoadTypeLibEx(L"COMmunicator.exe\\101", REGKIND_REGISTER, &pTypeLib);
	pTypeLib->Release();
	RegisterServer("COMmunicator.exe", CLSID_COMmunicator, "COMmunicator Test Task Server", SERVER_PROGID, SERVER_PROGID SERVER_VERS, 0);
	if(argc < 2)
	{
		cout << "Server Regestered." << endl;
		exit(true);
	}
	size_t fargSize = strlen(argv[1]) + 1;
	char *pFarg = (char*)malloc(fargSize);
	strcpy_s(pFarg, fargSize, argv[1]);
	char* pContext = pFarg;
	char* szToken = strtok_s(argv[1], "-/", &pContext);
	if(_stricmp(szToken, "u") == 0)
	{
		UnRegisterTypeLib(LIBID_COMmunicatorLib, 1, 0, LANG_NEUTRAL, SYS_WIN32);
		UnregisterServer(CLSID_COMmunicator, SERVER_PROGID, SERVER_PROGID ".1");
		cout << "Server Unregestered." << endl;
		exit(true);
	}
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	g_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	DWORD dwRegister;
	IClassFactory *pIFactory = new CFactory();
	CoRegisterClassObject(CLSID_COMmunicator, pIFactory, CLSCTX_LOCAL_SERVER, REGCLS_SUSPENDED|REGCLS_MULTIPLEUSE, &dwRegister);
	cout << "Out-of-process Dual-interface DCOM Server Started." << endl;
	CoResumeClassObjects(); 
	WaitForSingleObject(g_hEvent, INFINITE);
	CloseHandle(g_hEvent);
	CoRevokeClassObject(dwRegister);
	pIFactory->Release();
	CoUninitialize();
	cout << "Server Exited." << endl;
}

#undef UNICODE
#undef _UNICODE
#include <objbase.h>
BOOL AddKey(const char* pszPath, const char* szSubkey, const char* szValue);
BOOL SetKey(const char* szKey, const char* szNamedValue, const char* szValue);
void GuidToStr(REFCLSID clsid, char* szCLSID, int length);
LONG DeleteKey(HKEY hKeyParent, const char* szKeyChild);

HRESULT RegisterLocalServer(const char* szModuleName, REFCLSID clsid, const char* szFriendlyName,
   const char* szVerIndProgID, const char* szProgID, const char* szThreadingModel)
{
	char szModule[512];
	HMODULE hModule = GetModuleHandle(szModuleName);
	DWORD dwResult = GetModuleFileName(hModule, szModule, sizeof(szModule)/sizeof(char));
	char szCLSID[39];
	GuidToStr(clsid, szCLSID, sizeof(szCLSID));
	char szKey[64];
	strcpy(szKey, "CLSID\\");
	strcat(szKey, szCLSID);
	AddKey(szKey, NULL, szFriendlyName);
	if(strstr(szModuleName, ".exe") == NULL)
	{
		AddKey(szKey, "InprocServer32", szModule);
		char szInproc[64];
		strcpy(szInproc, szKey);
		strcat(szInproc, "\\InprocServer32");
		SetKey(szInproc, "ThreadingModel", szThreadingModel);
	}
	else
		AddKey(szKey, "LocalServer32", szModule);
	AddKey(szKey, "ProgID", szProgID);
	AddKey(szKey, "VersionIndependentProgID", szVerIndProgID);
	AddKey(szVerIndProgID, NULL, szFriendlyName);
	AddKey(szVerIndProgID, "CLSID", szCLSID);
	AddKey(szVerIndProgID, "CurVer", szProgID);
	AddKey(szProgID, NULL, szFriendlyName);
	AddKey(szProgID, "CLSID", szCLSID);
	return S_OK;
}

LONG UnregisterServer(REFCLSID clsid, const char* szVerIndProgID, const char* szProgID)
{
	char szCLSID[39];
	GuidToStr(clsid, szCLSID, sizeof(szCLSID));
	char szKey[64];
	strcpy(szKey, "CLSID\\");
	strcat(szKey, szCLSID);
	LONG lResult = DeleteKey(HKEY_CLASSES_ROOT, szKey);
	lResult = DeleteKey(HKEY_CLASSES_ROOT, szVerIndProgID);
	lResult = DeleteKey(HKEY_CLASSES_ROOT, szProgID);
	return S_OK;
}

void GuidToStr(REFCLSID clsid, char* szCLSID, int length)
{
	LPOLESTR wszCLSID = NULL;
	HRESULT hr = StringFromCLSID(clsid, &wszCLSID);
	wcstombs(szCLSID, wszCLSID, length);
	CoTaskMemFree(wszCLSID);
}

LONG DeleteKey(HKEY hKeyParent, const char* lpszKeyChild)
{
	HKEY hKeyChild;
	LONG lRes = RegOpenKeyEx(hKeyParent, lpszKeyChild, 0, KEY_ALL_ACCESS, &hKeyChild);
	if(lRes != ERROR_SUCCESS) return lRes;
	FILETIME time;
	char szBuffer[256];
	DWORD dwSize = 256;
	while(RegEnumKeyEx(hKeyChild, 0, szBuffer, &dwSize, NULL, NULL, NULL, &time) == S_OK)
	{
		lRes = DeleteKey(hKeyChild, szBuffer);
		if(lRes != ERROR_SUCCESS)
		{
			RegCloseKey(hKeyChild);
			return lRes;
		}
		dwSize = 256;
	}
	RegCloseKey(hKeyChild);
	return RegDeleteKey(hKeyParent, lpszKeyChild);
}

BOOL AddKey(const char* szKey, const char* szSubkey, const char* szValue)
{
	HKEY hKey;
	char szKeyBuf[1024];
	strcpy(szKeyBuf, szKey);
	if(szSubkey != NULL)
	{
		strcat(szKeyBuf, "\\");
		strcat(szKeyBuf, szSubkey );
	}
	long lResult = RegCreateKeyEx(HKEY_CLASSES_ROOT, szKeyBuf, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if(lResult != ERROR_SUCCESS) return FALSE;
	if(szValue != NULL)
		RegSetValueEx(hKey, NULL, 0, REG_SZ, (BYTE *)szValue, strlen(szValue)+1);
	RegCloseKey(hKey);
	return TRUE;
}

BOOL SetKey(const char* szKey, const char* szNamedValue, const char* szValue)
{
	HKEY hKey;
	char szKeyBuf[1024];
	strcpy(szKeyBuf, szKey);
	long lResult = RegOpenKeyEx(HKEY_CLASSES_ROOT, szKeyBuf, 0, KEY_SET_VALUE, &hKey);
	if(lResult != ERROR_SUCCESS) return FALSE;
	if(szValue != NULL)
		RegSetValueEx(hKey, szNamedValue, 0, REG_SZ, (BYTE*)szValue, strlen(szValue)+1);
	RegCloseKey(hKey);
	return TRUE;
}