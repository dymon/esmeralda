#define _WIN32_DCOM
#include <iostream>

using namespace std;

#include "COMmunicator_h.h"
HANDLE g_hEvent;

class COMmunicator : public ISHMEMserver, public ISHMEMclient
{
	long m_cRef;
	ITypeInfo* m_pTypeInfo;
public:
	COMmunicator() : m_cRef(1) { CoAddRefServerProcess();};
	~COMmunicator() { if(CoReleaseServerProcess() == 0) SetEvent(g_hEvent); };

	ULONG __stdcall AddRef() { return InterlockedIncrement(&m_cRef); };
	ULONG __stdcall Release();
	HRESULT __stdcall QueryInterface(REFIID iid, void** ppv);

	HRESULT __stdcall GetTypeInfoCount(UINT* pCountTypeInfo) {  *pCountTypeInfo = 1;  return S_OK; };
	HRESULT __stdcall GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo** ppITypeInfo);
	HRESULT __stdcall GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames, LCID lcid, DISPID* rgDispId)
   { return DispGetIDsOfNames(m_pTypeInfo, rgszNames, cNames, rgDispId); };
	HRESULT __stdcall Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pDispParams, VARIANT* pVarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr)
   { return DispInvoke(this, m_pTypeInfo, dispIdMember, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr); }
	// ISHMEMid
	STDMETHOD_(LPWSTR, GetSharedMemoryName)();
	// ISHMEMserver
	STDMETHOD(InitializeServer)(ISHMEMserverCallback *pUnk);
	STDMETHOD(FinalizeServer)();
	// ISHMEMclient
	STDMETHOD(InitializeClient)(
	    DWORD   dwChunkSize,
	    DWORD * pdwClientID
	);
	STDMETHOD(EnqueueFile)(
	    DWORD   dwClientID,
	    LPCWSTR szFileName
	);
	STDMETHOD(FinalizeClient)(
	    DWORD   dwClientID
	);
};
