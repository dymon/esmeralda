#include <iostream>
#include <windows.h>

typedef const char *cstr;
typedef unsigned short *ustr;
typedef unsigned int uint;

char GenRandomChar();
std::wstring wideString(cstr c = '\0');
ustr toUniString(cstr c = '\0');
std::wstring GetNewShareName();

