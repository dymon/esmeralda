#include "serviceLibCommon.h"

bool initialized = false;

bool InitServiceLib() {
	srand(::GetTickCount());
	return true;
}
 
char GenRandomChar() {
	if (!initialized) initialized = InitServiceLib();
    static cstr alphanum =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
	uint m = 62;
    return alphanum[rand() % m];
}
 
std::wstring wideString(cstr c){
        const size_t cSize = strlen(c) + 1;
        std::wstring wc(cSize, L'#');
        mbstowcs(&wc[0], c, cSize);
        return wc;
}

ustr toUniString(cstr c) {
	uint strLen = strlen(c);
	if (!strLen) return NULL;
	char *pSrc = (char *)c;
	int nLen = MultiByteToWideChar(CP_ACP, 0, pSrc, strLen,
		NULL, NULL
	);
	ustr Result = (ustr)malloc(nLen << 1);
	MultiByteToWideChar(CP_ACP, 0, pSrc, strLen,
		(LPWSTR)Result, nLen
	);
	return Result;
};
 
std::wstring GetNewShareName() {
        char name[15] = "shmem";
		for (uint i = 5; i < 15; i++) name[i] = GenRandomChar();
		name[14] = 0;
        std::wstring wsname = wideString(name);
        return wsname;
}

const int GetHash(char * filename, int clientId) {
        char buff[256];
        char *clientIdStr = itoa(clientId, buff, 10);
 
        std::string str(filename);
        str.append(clientIdStr);
 
        int hash = 0;//std::hash<std::string>()(str);
       
        return hash;
}
 

